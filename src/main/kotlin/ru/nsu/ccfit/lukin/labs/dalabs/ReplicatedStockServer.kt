package ru.nsu.ccfit.lukin.labs.dalabs

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import com.xenomachina.argparser.mainBody
import org.jgroups.JChannel
import org.jgroups.ReceiverAdapter
import org.jgroups.View
import org.jgroups.blocks.RequestOptions
import org.jgroups.blocks.RpcDispatcher
import org.jgroups.blocks.locking.LockService
import org.jgroups.util.Rsp
import org.jgroups.util.RspList
import org.jgroups.util.Util

import java.io.*
import java.util.HashMap

/**
 * Replicated stock server; every cluster node has the same state (stocks).
 */
class ReplicatedStockServer @Throws(Exception::class) constructor(properties: String) : ReceiverAdapter() {
    private val stocks = HashMap<String, Double>()
    private val channel: JChannel = JChannel(properties)
    private val lockService: LockService = LockService(channel)
    private val rpcDispatcher: RpcDispatcher = RpcDispatcher(channel, this)
        .setMembershipListener(this)
        .setStateListener(this)

    /**
     * Assigns a value to a stock
     */
    fun _setStock(name: String, value: Double) {
        synchronized(stocks) {
            stocks[name] = value
            System.out.printf("-- set %s to %s\n", name, value)
        }
    }

    /**
     * Removes a stock from the HashMap
     */
    fun _removeStock(name: String) {
        synchronized(stocks) {
            stocks.remove(name)
            System.out.printf("-- removed %s\n", name)
        }
    }

    /**
     * Get a stock from the HashMap
     */
    fun _getStock(name: String): Double? {
        synchronized(stocks) {
            val `val` = stocks[name]
            System.out.printf("-- returned %s with %s\n", name, `val`)
            return `val`
        }
    }

    /**
     * Set a stock if it's value equal to ref
     */
    fun _casStock(name: String, reference: Double, newValue: Double): Boolean {
        synchronized(stocks) {
            val value = stocks[name]
            return if (value == reference) {
                stocks[name] = newValue
                System.out.printf("-- cas -- set %s to %s\n", name, newValue)
                true
            } else {
                System.out.printf("-- cas -- failed in setting %s to %s\n", name, newValue)
                false
            }
        }
    }

    @Throws(Exception::class)
    fun start() {
        channel.connect("stocks")
        rpcDispatcher.start<RpcDispatcher>()
        channel.getState(null, 30000) // fetches the state from the coordinator
        while (true) {
            val c = Util.keyPress("[1] Show stocks [2] Get quote [3] Set quote [4] Remove quote [5] CAS [x] Exit")
            try {
                when (c) {
                    '1'.toInt() -> showStocks()
                    '2'.toInt() -> getStock()
                    '3'.toInt() -> setStock()
                    '4'.toInt() -> removeStock()
                    '5'.toInt() -> casStock()
                    'x'.toInt() -> {
                        channel.close()
                        return
                    }
                }
            } catch (ex: Exception) {
                println(ex.toString())
            }

        }
    }


    override fun viewAccepted(view: View) {
        println("-- VIEW: $view")
    }

    @Throws(Exception::class)
    override fun getState(output: OutputStream) {
        val out = DataOutputStream(output)
        synchronized(stocks) {
            println("-- returning " + stocks.size + " stocks")
            Util.objectToStream(stocks, out)
        }
    }


    @Throws(Exception::class)
    override fun setState(input: InputStream) {
        val inStream = DataInputStream(input)
        val newState = Util.objectFromStream<Map<String, Double>>(inStream)
        println("-- received state: " + newState.size + " stocks")
        synchronized(stocks) {
            stocks.clear()
            stocks.putAll(newState)
        }
    }


    @Throws(IOException::class)
    private fun getStock() {
        val ticker = readString("Symbol")
        val lock = lockService.getLock("lock$ticker")
        lock.lock()
        try {
            synchronized(stocks) {
                val `val` = stocks[ticker]
                println("$ticker is $`val`")
            }
        } finally {
            lock.unlock()
        }
    }

    @Throws(Exception::class)
    private fun setStock() {
        val ticker: String = readString("Symbol")
        val value: String = readString("Value")
        val lock = lockService.getLock("lock$ticker")
        lock.lock()
        try {
            val rspList = rpcDispatcher.callRemoteMethods<Void>(
                null, "_setStock", arrayOf(ticker, java.lang.Double.parseDouble(value)),
                arrayOf(String::class.java, Double::class.java), RequestOptions.SYNC()
            )
            println("rspList:\n$rspList")
        } finally {
            lock.unlock()
        }
    }


    @Throws(Exception::class)
    private fun removeStock() {
        val ticker = readString("Symbol")
        val lock = lockService.getLock("lock$ticker")
        lock.lock()
        try {
            val rspList = rpcDispatcher.callRemoteMethods<Void>(
                null, "_removeStock", arrayOf<Any>(ticker),
                arrayOf<Class<*>>(String::class.java), RequestOptions.SYNC()
            )
            println("rspList:\n$rspList")
        } finally {
            lock.unlock()
        }
    }

    @Throws(Exception::class)
    private fun casStock() {
        val ticker = readString("Symbol")
        val oldValue = java.lang.Double.parseDouble(readString("Old value"))
        val `val` = readString("New value")

        val lock = lockService.getLock("lock$ticker")
        lock.lock()
        try {
            System.out.printf("Old value: %s", oldValue)

            val rspList : RspList<Boolean> = rpcDispatcher.callRemoteMethods<Boolean>(
                null,
                "_casStock",
                arrayOf(ticker, oldValue, java.lang.Double.parseDouble(`val`)),
                arrayOf<Class<*>>(String::class.java, Double::class.javaPrimitiveType!!, Double::class.java),
                RequestOptions.SYNC()
            )

            var countSucceeded = 0

            for (v : Rsp<Boolean> in rspList) {
                if (v.wasReceived() && v.value) {
                    countSucceeded++
                }
            }

            val succeeded = countSucceeded > rspList.numReceived() / 2

            println("cas: $ticker set to $`val`${if (succeeded) " successfully" else " FAILED"}")
        } finally {
            lock.unlock()
        }
    }


    private fun showStocks() {
        println("Stocks:")
        synchronized(stocks) {
            for ((key, value) in stocks) {
                println("$key: $value")
            }
        }
    }

    @Throws(IOException::class)
    private fun readString(s: String): String {
        var c: Int
        var looping = true
        val sb = StringBuilder()
        print("$s: ")
        System.out.flush()
        val available = System.`in`.available().toLong()
        val skipped = System.`in`.skip(available)
        if (available != skipped) {
            System.err.println("available($available) != skipped($skipped)")
        }

        while (looping) {
            c = System.`in`.read()
            when (c) {
                -1, '\n'.toInt(), 13 -> looping = false
                else -> sb.append(c.toChar())
            }
        }

        return sb.toString()
    }
}
class ServerArgs(parser: ArgParser) {
    val props by parser.storing(
        "-p", "--props",
        help = "properties file"
    ).default("conf/config.xml")
}

fun main(args: Array<String>) = mainBody {
    ArgParser(args).parseInto(::ServerArgs).run {
        ReplicatedStockServer(props).start()
    }
}
