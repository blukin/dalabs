package ru.nsu.ccfit.lukin.labs.dalabs

import com.sun.deploy.trace.Trace.flush
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import com.xenomachina.argparser.mainBody
import org.jgroups.JChannel
import org.jgroups.Message
import org.jgroups.ReceiverAdapter
import org.jgroups.View
import org.jgroups.jmx.JmxConfigurator
import org.jgroups.util.Util

import java.io.BufferedReader
import java.io.InputStreamReader

class Chat @Throws(Exception::class)
constructor(name: String, properties: String) : ReceiverAdapter() {
    private var channel: JChannel = JChannel(properties).name(name).receiver(this)

    override fun viewAccepted(new_view: View) {
        println("\r** view: $new_view")
        print("> ")
        flush()

    }

    override fun receive(msg: Message) {
        val payload = msg.getObject<String>()
        System.out.printf("\r[%s]: %s\n", msg.src, payload)
        print("> ")
        flush()
    }


    @Throws(Exception::class)
    fun start() {
        channel.connect("ChatCluster")
        JmxConfigurator.registerChannel(channel, Util.getMBeanServer(), "chat-channel", channel.clusterName, true)
        eventLoop()
        channel.close()
    }

    private fun eventLoop() {
        val `in` = BufferedReader(InputStreamReader(System.`in`))
        while (true) {
            try {
                val line = `in`.readLine().toLowerCase()
                if (line.startsWith("quit") || line.startsWith("exit"))
                    break
                val msg = Message(null, line)
                channel.send(msg)
                print("> ")
                flush()
            } catch (e: Exception) {
            }

        }
    }
}

class ChatArgs(parser: ArgParser) {
    val props by parser.storing(
        "-p", "--props",
        help = "properies file"
    ).default("conf/config.xml")

    val name by parser.storing(
        "-n", "--name",
        help = "name of the user"
    )
}

fun main(args: Array<String>) = mainBody {
    ArgParser(args).parseInto(::ChatArgs).run {
        Chat(name, props).start()
    }
}
